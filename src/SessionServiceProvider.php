<?php

namespace idfortysix\mongosessionhandler;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\ConnectionInterface;
use Config;
use idfortysix\mongosessionhandler\MongoSessionHandler;


class SessionServiceProvider extends ServiceProvider
{
    protected $defer = false;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(ConnectionInterface $connection)
    {  
        Session::extend('mongo', function ($app) use ($connection){
            $table   = Config::get('session.table');
            $minutes = Config::get('session.lifetime');
            return new MongoSessionHandler($connection, $table, $minutes);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
         
    }
}
