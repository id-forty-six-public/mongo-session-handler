<?php

namespace idfortysix\mongosessionhandler;

use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Support\Arr;

// Keiciam session storage is file i mongodb, del to extendinam vendor methodus, kad id pakeist i _id 
class MongoSessionHandler extends DatabaseSessionHandler
{
   
    public function read($sessionId)
    {
		return parent::read(md5($sessionId));
    }
    
    protected function performInsert($sessionId, $payload)
    {
        try {
            return $this->getQuery()->insert(Arr::set($payload, '_id', md5($sessionId)));
        } catch (QueryException $e) {
            $this->performUpdate($sessionId, $payload);
        }
    }
    
    protected function performUpdate($sessionId, $payload)
    {
        return $this->getQuery()->where('_id', md5($sessionId))->update($payload);
    }
    
    public function destroy($sessionId)
    {
        $this->getQuery()->where('_id', md5($sessionId))->delete();

        return true;
    }
    
}
